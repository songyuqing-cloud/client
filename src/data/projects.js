import hakimAppImg from './HakimApp.jpg'

const projects = [
  {
    id: 'pr1',
    title: 'Workout Booking App',
    description: 'A booking app made for coach Hakim Azahar.',
    tags: ['Fullstack', 'ReactJs', 'Node.js'],
    image: hakimAppImg,
    gitlab: 'https://gitlab.com/bookworkoutclassapp',
    behance: '#',
    dribbble: '#'
  },
  {
    id: 'pr2',
    title: 'Portfolio Website',
    description: 'Portfolio website to present my work.',
    tags: ['Frontend', 'ReactJs', 'Material UI'],
    image: '#',
    gitlab: '#',
    behance: '#',
    dribbble: '#'
  }
]

export default projects
