import React from 'react'
import style from './Portfolio.module.css'
import projects from '../../data/projects'
import ProjectCard from '../../components/ProjectCard/ProjectCard'

function Portfolio () {
  return (
    <div className={style.container}>
      <h2 className={style.title}>&mdash; Projects</h2>
      <div className={style.projectsContainer}>
        {projects.map(project => (
          <ProjectCard key={project.id} title={project.title} description={project.description} tags={project.tags} image={project.image} />
        ))}
      </div>
    </div>
  )
}

export default Portfolio
