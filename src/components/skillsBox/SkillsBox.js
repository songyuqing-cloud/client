import React from 'react'
import style from './SkillsBox.module.css'
import { IoStatsChart } from 'react-icons/io5'
import { Link } from 'react-router-dom'

function SkillsBox () {
  return (
    <Link to='/skills'>
      <div className={style.backdrop} />
      <div className={style.container}>
        <p>Skills</p>
        <IoStatsChart
          color='#EB4A4A'
          style={{
            marginLeft: '8px'
          }}
        />
        <IoStatsChart
          color='#EB4A4A'
          style={{
            position: 'relative',
            left: '-3px'
          }}
        />
      </div>
    </Link>

  )
}

export default SkillsBox
